## Regras Customizadas pt-br ##


***Clonar as regras no diretório do spamassassim do zimbra e fazer um link simbólico para o custom_br.cf***
### ZIMBRA <= 8.0 ###
DIR='/opt/zimbra/conf/spamassassin'
### ZIMBRA > 8.0 ###
DIR='/opt/zimbra/data/spamassassin/localrules'  

## ##
---
cd $DIR

git clone https://atua@bitbucket.org/atua/zimbra-amavis-regras_br.git

ln -s zimbra-amavis-regras_br/custom_br.cf